import { fetchApi } from "../apiFactory";
import { TEST } from "../constants";
import { transformDataApi } from "./transform";
const { GET_LIST, ADD_ITEM, DELETE_ITEM, UPDATE_ITEM, SEARCH_ITEM } = TEST;

export const testApi = () => {
  const getList = async (params) => {
    // TODO: use transform here
    const result = await fetchApi(GET_LIST, params);
    const transformData = transformDataApi(result);
    return transformData;
  };
  const addItem = async (obj) => {
    const data = { name: obj.name, date: obj.date };
    return await fetchApi(ADD_ITEM, {}, data);
  };
  const deleteItem = async (obj) => {
    const data = { id: obj._id };
    return await fetchApi(DELETE_ITEM, data);
  };
  const updateItem = async (obj) => {
    const data = {
      isActive: obj.isActive,
      date: obj.date,
      password: obj.password,
    };
    return await fetchApi(UPDATE_ITEM, { id: obj._id }, data);
  };
  const updateActive = async (obj) => {
    const data = { isActive: !obj.isActive };
    return await fetchApi(UPDATE_ITEM, { id: obj._id }, data);
  };
  const searchData = async (obj) => {
    const result = await fetchApi(SEARCH_ITEM, obj );
    const transformData = transformDataApi(result);
    return transformData
  };
  return {
    getList,
    addItem,
    deleteItem,
    updateItem,
    updateActive,
    searchData,
  };
};
