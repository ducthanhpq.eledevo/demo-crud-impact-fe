// Create transform data function
export const transformDataApi = (data) => {
  if (!data) {
    return;
  } else {
    const listAccount = data.result.map((items, key) => ({
      STT: key + 1,
      _id: items._id,
      isActive: items.isActive,
      name: items.name,
      time: items.time,
      isActive: items.isActive,
      password: items.password,
      date: items.date,
    }));
    const totalElement = data.totalElement;
    const totalPage = data.totalPage;
    return { listAccount, totalElement, totalPage };
  }
};
