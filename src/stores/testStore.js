import { defineStore } from "pinia";
import { reactive, ref } from "vue";

import { testApi } from "@/apis";
import { LIMIT } from "@/utils/constants";

export const useTestStore = defineStore("testStore", () => {
  const listTest = ref([]);
  const totalElement = ref(0);
  const totalPage = ref(0);
  const currentPage = ref(0);
  const totalPageNew = ref(0);

  const getDataTest = async (params) => {
    const data = { page: params, limit: LIMIT };
    currentPage.value = params;
    const { getList } = testApi(data);
    const res = await getList(data);

    listTest.value = res.listAccount;
    totalElement.value = res.totalElement;
    totalPage.value = res.totalPage;
  };
  const addDataTest = async (obj) => {
    const data = { name: obj.name, date: obj.date };
    const { addItem } = testApi(data);
    const { totalRecord } = await addItem(data);
    totalPageNew.value = Math.ceil(totalRecord / LIMIT);
    currentPage.value = 1;
  };
  const deleteDataTest = async (data) => {
    const { deleteItem } = testApi(data);
    const { totalRecord } = await deleteItem(data);
    totalPageNew.value = Math.ceil(totalRecord / LIMIT);
    currentPage.value =
      currentPage.value > totalPageNew.value && totalPageNew.value !== 0
        ? currentPage.value - 1
        : currentPage.value;
  };
  const updateDataTest = async (data) => {
    const { updateItem } = testApi(data);
    await updateItem(data);
  };
  const updateActiveTest = async (data) => {
    const { updateActive } = testApi(data);
    await updateActive(data);
  };
  const searchDataTest = async (obj) => {
    const data = {
      textSearch: obj.textSearch,
      page: obj.page,
      limit: LIMIT,
    };
    currentPage.value = obj.page;
    const { searchData } = testApi(data);
    const res = await searchData(data);
    listTest.value = res.listAccount;
    totalElement.value = res.totalElement;
  };
  return {
    listTest,
    totalElement,
    totalPage,
    currentPage,
    totalPageNew,
    getDataTest,
    addDataTest,
    deleteDataTest,
    updateDataTest,
    updateActiveTest,
    searchDataTest,
  };
});
