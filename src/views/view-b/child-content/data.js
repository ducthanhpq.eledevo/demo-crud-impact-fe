import moment from "moment/moment";
export const columns = [
  {
    title: "STT",
    key: "STT",
    dataIndex: "STT",
    align: "center",
  },
  {
    title: "Tên đăng nhập",
    key: "name",
    dataIndex: "name",
    align: "center",
  },
  {
    title: "Ngày tạo",
    key: "time",
    dataIndex: "time",
    align: "center",
    customRender: ({text, record}) => moment(record.createData).format('L')
  },
  {
    title: "trạng thái",
    key: "status",
    dataIndex: "status",
    align: "center",
  },
  {
    title: "Thao tác",
    dataIndex: "operation",
    key:"operation",
    align: "center",
  },
];

export const formItemLayout = {
  labelCol: {
      xs: { span: 24, },
      sm: { span: 8, },
  },
  wrapperCol: {
      xs: { span: 24, },
      sm: { span: 16, },
  },
};