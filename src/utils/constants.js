export const DOMAIN_DEVELOVPMENT = "http://localhost:3001";

export const TABLE_SIZE = {
  default: "default",
  small: "small",
  medium: "middle",
  large: "large",
};
export const mode = {
  CREATE: 'CREATE',
  EDIT: 'EDIT',
  VIEW: 'VIEW'
}
export const dateFormatList = ['DD/MM/YYYY', 'DD/MM/YY'];
export const LIMIT = 10;
