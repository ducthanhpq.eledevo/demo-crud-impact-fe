import { ref } from "vue";

export const useLoading = () => {
  const isLoading = ref(false);

  const setLoading = (status) => {
    isLoading.value = status;
  };

  return {
    isLoading,
    setLoading,
  };
};

export const useModal = () => {
  const isVisible = ref(false);

  const showModal = () => (isVisible.value = true);

  const hideModal = () => (isVisible.value = false);

  return {
    isVisible,
    showModal,
    hideModal,
  };
};

export const useFlag = () =>{
  const title = ref ("")
  const view = "Xem tài khoản"
  const create = "Thêm tài khoản"
  const edit = "Sửa tài khoản"
  const statusView = () => (title.value = view)
  const statusEdit = () => (title.value = edit)
  const statusCreate = () => (title.value = create)
  return{
    title,
    view,
    create,
    edit,
    statusView,
    statusEdit,
    statusCreate
  }
}